import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'

import{
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom"
import Home from './pages/home.jsx'
import Profil from './pages/profil.jsx'
import Wishlist from './pages/wishlist.jsx'
import Gift from './pages/gift.jsx'

const router = createBrowserRouter([
  {
    path:"/",
    element:<Home/>
  },
  {
    path:"/profil",
    element:<Profil/>
  },
  {
    path:"/wishlist",
    element:<Wishlist/>
  },
  {
    path:"/gift",
    element:<Gift/>
  }
  
])


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
     <RouterProvider router={router} />
  </React.StrictMode>,
)
